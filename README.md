# test-md-anchors


<!-- !toc -->

  * [Jack & Jim](#markdown-header-jack-jim)
  * [Dash - and - minus](#markdown-header-dash-and-minus)
  * [class~method](#markdown-header-classmethod)
  * [condense consecutive | = hyphens](#markdown-header-condense-consecutive-hyphens)
  * [create object (post /db/create)](#markdown-header-create-object-post-dbcreate)
  * [does not remove escape codes instead removes % as in %3Cstatic%E3 %86 %3Cstatic%E3 coreappupevents %E2%86%92 object](#markdown-header-does-not-remove-escape-codes-instead-removes-as-in-3cstatice3-86-3cstatice3-coreappupevents-e28692-object)
  * [func($event)](#markdown-header-funcevent)
  * [furnace "updateSlot" (oldItem, newItem)](#markdown-header-furnace-updateslot-olditem-newitem)
  * [Jack & Jill](#markdown-header-jack-jill)
  * [mineflayer.windows.Window (base class)](#markdown-header-mineflayerwindowswindow-base-class)
  * [module-specific-variables-using-jsdoc-@module](#markdown-header-module-specific-variables-using-jsdoc-module)
  * [My Cool@Header](#markdown-header-my-coolheader)
  * ["playerJoined" (player)](#markdown-header-playerjoined-player)
  * [preserve consecutive | = hyphens](#markdown-header-preserve-consecutive-hyphens)
  * [proxyquire(request: String, stubs: Object)](#markdown-header-proxyquirerequest-string-stubs-object)
  * [remove {curly braces}{}](#markdown-header-remove-curly-braces)
  * [remove escape codes %3Cstatic%E3 coreappupevents %E2%86%92 object](#markdown-header-remove-escape-codes-3cstatice3-coreappupevents-e28692-object)
  * [remove exclamation point!](#markdown-header-remove-exclamation-point)
  * [remove lt and gt <static>mycall](#markdown-header-remove-lt-and-gt-staticmycall)
  * [remove ++++pluses+](#markdown-header-remove-pluses)
  * [remove ;;semi;colons](#markdown-header-remove-semicolons)
  * [remove = sign](#markdown-header-remove-sign)
  * [remove special chars \`!@#%^&\*()-+=\[{\]}\|;:'",<.>/?](#markdown-header-remove-special-chars-9642-9193124)
  * [replace $ with d and ~ with t](#markdown-header-replace-with-d-and-with-t)
  * [replace – or —](#markdown-header-replace-or)
  * [trailing *](#markdown-header-trailing)
  * ['webdav' Upload Method for 'dput'](#markdown-header-webdav-upload-method-for-dput)
  * [where is it?](#markdown-header-where-is-it)
  * [window.findInventoryItem(itemType, metadata, \[notFull\])](#markdown-header-windowfindinventoryitemitemtype-metadata-91notfull93)
  * [**Bold** text #1: _using_ the `-s` option](#markdown-header-bold-text-1-using-the-s-option)
  * [`history [pgn | alg]`](#markdown-header-history-pgn-alg)
  * [https://github.com/commenthol/markedpp](#markdown-header-httpsgithubcomcommentholmarkedpp)
  * [<https://github.com/commenthol/markedpp>](#markdown-header-httpsgithubcomcommentholmarkedpp_1)
  * [A link to](#markdown-header-a-link-to)
  * [A reference to](#markdown-header-a-reference-to)
  * [markedpp](#markdown-header-markedpp)
  * [&lt;html &amp; entities&gt;](#markdown-header-lthtml-amp-entitiesgt)
  * [&#60;xml &amp; entities &#8710;&#62;](#markdown-header-60xml-amp-entities-871062)
  * [存在，【中文】；《标点》、符号！的标题？](#markdown-header-)
  * [В чащах юга жил бы цитрус?](#markdown-header-_1)
  * [דג סקרן שט בים מאוכזב ולפתע מצא לו חברה](#markdown-header-_2)
  * [Quiere la boca exhausta vid, kiwi, piña y fugaz jamón](#markdown-header-quiere-la-boca-exhausta-vid-kiwi-pina-y-fugaz-jamon)
  * ["Fix, Schwyz!" quäkt Jürgen blöd vom Paß](#markdown-header-fix-schwyz-quakt-jurgen-blod-vom-pa)
  * [Modu📦les 😀](#markdown-header-modules)
  * [Modu📦les😀](#markdown-header-modules_1)
  * [😀 Modu📦les](#markdown-header-modules_2)
  * [Mo📦du📦les](#markdown-header-modules_3)
  * [1. One](#markdown-header-1-one)
  * [2.1\. Two One](#markdown-header-2146-two-one)
  * [1.2.3.4.5.6\. 1 2 3 4 5 6](#markdown-header-12345646-1-2-3-4-5-6)
  * [Same Heading](#markdown-header-same-heading)
  * [Same Heading](#markdown-header-same-heading_1)
  * [Same Heading](#markdown-header-same-heading_2)
  * [标题](#markdown-header-_3)
  * [标题](#markdown-header-_4)

<!-- toc! -->

----

snip-snip-snip

----

**Special Chars**

## Jack & Jim

## Dash - and - minus

## class~method

## condense consecutive | = hyphens

## create object (post /db/create)

## does not remove escape codes instead removes % as in %3Cstatic%E3 %86 %3Cstatic%E3 coreappupevents %E2%86%92 object

## func($event)

## furnace "updateSlot" (oldItem, newItem)

## Jack & Jill

## mineflayer.windows.Window (base class)

## module-specific-variables-using-jsdoc-@module

## My Cool@Header

## "playerJoined" (player)

## preserve consecutive | = hyphens

## proxyquire(request: String, stubs: Object)

## remove {curly braces}{}

## remove escape codes %3Cstatic%E3 coreappupevents %E2%86%92 object

## remove exclamation point!

## remove lt and gt <static>mycall

## remove ++++pluses+

## remove ;;semi;colons

## remove = sign

## remove special chars \`!@#%^&\*()-+=\[{\]}\|;:'",<.>/?

## replace $ with d and ~ with t

## replace – or —

## trailing *

## 'webdav' Upload Method for 'dput'

## where is it?

## window.findInventoryItem(itemType, metadata, \[notFull\])

----

**Markdown**

## **Bold** text #1: _using_ the `-s` option

## `history [pgn | alg]`

## https://github.com/commenthol/markedpp

## <https://github.com/commenthol/markedpp>

## [A link to](https://github.com/commenthol/markedpp)

## [A reference to][]

## [markedpp][]

[markedpp]: https://github.com/commenthol/markedpp

----

**HTML-XML entities**

https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references

## &lt;html &amp; entities&gt;

## &#60;xml &amp; entities &#8710;&#62;

----

**Unicode**

https://es.wikipedia.org/wiki/Pangrama

## 存在，【中文】；《标点》、符号！的标题？

## В чащах юга жил бы цитрус?

## דג סקרן שט בים מאוכזב ולפתע מצא לו חברה

## Quiere la boca exhausta vid, kiwi, piña y fugaz jamón

## "Fix, Schwyz!" quäkt Jürgen blöd vom Paß

----

**Emojis**

https://www.unicode.org/emoji/charts/full-emoji-list.html

## Modu📦les 😀

## Modu📦les😀

## 😀 Modu📦les

## Mo📦du📦les

----

**Numbered headings**

## 1. One

## 2.1\. Two One

## 1.2.3.4.5.6\. 1 2 3 4 5 6

----

**Identical headings**

## Same Heading

## Same Heading

## Same Heading

## 标题

## 标题

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.

.


